package application;

import java.util.ArrayList;

public class Invoice {

    // attributes
    private ArrayList<Product> products;
    private double total;

    //Constructor
    public Invoice(ArrayList<Product> products, double total) {
        this.products = products;
        this.total = total;

    }

    /**
     * get and set the data from supplied file.
     * @return
     */
    // Methods - getters and setters
    public  ArrayList<Product> getProducts(){
        return products;
    }
    // Method - getter and setters
    public void setProducts(ArrayList<Product> products){
        this.products = products;
    }

    // Method - getter and setters
    public  double getTotal(){
        return total;
    }

    // Method - getter and setters
    public void setTotal(double total){
        this.total = total;
    }

    /**
     * convert to string the data that comes from supplied file.
     * @return the data as a string.
     */
    //Method
    public  String toString(){

        for (Product p : products){
            System.out.println(p.toString());
        }

        return "Invoice Total: $" + String.format("%.2f", getTotal()) + "\n";
    }

    static class Product {

        //Attributes
        private String name;
        private double price;

        //Constructor
        public Product(String name, double price){
            this.name = name;
            this.price = price;
        }

        /**
         * get and set the data from supplied file.
         * @return
         */
        //Methods Getter and setters
        public String getName(){
            return name;
        }
        public  void setName(String name){
            this.name = name;
        }
        public double getPrice(){
            return  price;
        }
        public void setPrice(double price){
            this.price = price;
        }

        /**
         * convert the integer data to string which in supplied file.
         * @return as a string.
         */
        //Method
        public String toString(){
            return getName() + ": " + "$" + String.format("%.2f", getPrice());
        }



    }


}
