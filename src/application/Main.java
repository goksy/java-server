package application;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    /**
     * Main performs to begin socket program
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        new MenuThread().start();

        ServerSocket serverSocket = new ServerSocket(6789);
        System.out.println("Listening on port 6789");



        while(true){
            Socket socket = serverSocket.accept();

            // new thread for client
            new EchoThread(socket).start();

            InetAddress ip = socket.getInetAddress();
            int port = socket.getPort();

            System.out.println("New client connected from: " + ip + ":" + port);
        }

    }

}
